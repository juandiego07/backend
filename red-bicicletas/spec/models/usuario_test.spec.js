var mongoose =  require('mongoose');
var bicicleta = require('../../models/bicicleta');
var usuario = require('../../models/usuario');
var reserva = require('../../models/reserva');

describe('Testing usuario', function(){
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('Error', console.error.bind(console, 'Connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            usuario.deleteMany({}, function(err, success) {
                if (err) console.log(err);
                bicicleta.deleteMany({},function(err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('Dede existir la reserva', (done) => {
            const usuario = new usuario({nombre: 'Juan'});
            usuario.save();
            const bicicleta = new bicicleta({code: 1, color: 'Verde', modelo: 'Urbana'});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);
            usuario.reserva(bicicleta.code, hoy, mañana, function(err, reserva) {
                reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reserva) {
                    console.log(reserva[0]);
                    expect(reserva.length).toBe(1);
                    expect(reserva[0].diasDeReserva()).toBe(2);
                    expect(reserva[0].bicicleta.code).toBe(1);
                    expect(reserva[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});