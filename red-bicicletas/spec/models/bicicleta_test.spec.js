var mongoose = require('mongoose');
var bicicleta =  require('../../models/bicicleta');

describe('Testing bicicleta', function () {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testDB';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db =  mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        bicicleta.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            done();
        });
    });

    describe('bicicleta.createInstance', () => {
        it('Crear un instancia de bicicleta', (done) => {
            var bici = bicicleta.createInstance(1, "Verde", "Urbana", [6.2699250625294125, -75.56353663756798]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toEqual(6.2699250625294125);
            expect(bici.ubicacion[1]).toEqual(-75.56353663756798);
            done();
        });
    });

    describe('bicicleta.Allbicis', () => {
        it('Comienza vacia', (done) => {
            bicicleta.Allbicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('bicicleta.Add', () => {
        it('Agrega solo una bicicleta', (done) => {
            var aBici = new bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            bicicleta.Add(aBici, function(err, newBici) {
                if(err) console.log(err);
                bicicleta.Allbicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('bicicleta.findByCode', () => {
        it('Debe devolver la bicicleta con code 1', (done) => {
            bicicleta.Allbicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                
                var aBici1 = new bicicleta({code: 1, color: "Negro", modelo: "Montaña"});
                bicicleta.Add(aBici1, function(err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new bicicleta({code: 2, color: "Azul", modelo: "Urbana"});
                    bicicleta.Add(aBici2, function(err, newBici) {
                        if (err) console.log(err);

                        bicicleta.findByCode(1, function (error, targetBici) {
                            expect(targetBici.code).toBe(aBici1.code);
                            expect(targetBici.color).toBe(aBici1.color);
                            expect(targetBici.modelo).toBe(aBici1.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });
});

// beforeEach(() => { bicicleta.Allbicis = [];})
// describe('bicicleta.Allbicis', () => {
//     it('Comienza vacio', () => {
//         expect(bicicleta.Allbicis.length).toBe(0);
//     });
// });

// describe('bicicleta.Add', () => {
//     it('Agregamos una bicicleta', () => {
//         expect(bicicleta.Allbicis.length).toBe(0);

//         var bici = new bicicleta(3, 'Rojo', 'Montaña', [6.304535669068921, -75.56512450510441]);
//         bicicleta.Add(bici);

//         expect(bicicleta.Allbicis.length).toBe(1);
//         expect(bicicleta.Allbicis[0]).toBe(bici);
//     });
// });

// describe('bicicleta.findById', () => {
//     it('Devuelve la bicicleta con ID: 1', () => {
//         expect(bicicleta.Allbicis.length).toBe(0);

//         var a = new bicicleta(1, 'Verde', 'Urbana', [6.270459938093382, -75.56358362381951]);
//         var b = new bicicleta(2, 'Azul', 'Urbana', [6.263890472627351, -75.56354070847553]);
//         bicicleta.Add(a);
//         bicicleta.Add(b);

//         var targetBici = bicicleta.findById(1);
        
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(a.color);
//         expect(targetBici.modelo).toBe(a.modelo);
//     });
// });