var request = require('request');
var bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');

var baseUrl = 'http://localhost:5000/api/bicicleta';

describe('bicicleta API', () => {

    beforeEach(function(done) {
        var mongoDB = 'mongoDB://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once(open, function() {
            console.log('We are connected to test database');
            done();
        });
    });
    
    afterEach( function(done)  {
        bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        })
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {

            request.get(baseUrl, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.estatusCode).toBe(200);
                expect(result.bicicleta.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code":10, "color": "Amarillo", "modelo": "Urbana", "lat": 6.298905072541963, "lng": , -75.55194949549335}'
            request.post({
                headers: headers,
                url: baseUrl + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("Amarillo");
                expect(bici.ubicacion).toBe(6.298905072541963);
                expect(bici.ubicacion).toBe(-75.55194949549335);
                done();
            });
        });
    });
});

    
