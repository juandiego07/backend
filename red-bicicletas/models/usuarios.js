var mongoose = require('mongoose');
var unique = require('mongoose-unique-validator');
var reserva =  require('./reserva');
var schema = mongoose.Schema;

const mailer = require('../mailer/mailer');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new schema({
    nombre: {
        type: String,
        trim: true,
        require: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        require: [true, 'El email es obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'El email es invalido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
        unique: true
    },
    password: {
        type: String,
        require: [true, 'El campo es obligatorio'],

    },
    passwordResetToken: String,
    passwordResetTokenExpire: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(unique, { message: 'El {PATH} ya existe con otro usuario'})

usuarioSchema.pre('save', function(next) {
    if(this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.method.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.method.reservar = function(biciId, desde, hasta, cb){
    var reserva = new reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta, hasta});
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.method.enviar_email_bienvenida =  function(cb) {
    const token =  new token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination =  this.email;
    token.save(function(err) {
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'no-replay@redbicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola\n\n' + 'Por favor para verificar su cuenta haga click en este enlace: \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return console.log(err.message); }
            console.log('A verification email has been sent to' + email_destination);
        });
    });

}

module.exports = mongoose.model('usuario', usuarioSchema);