// var bicicleta  = require("../controllers/bicicleta");
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var bicicletaSchema = new schema ({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
        
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion, ubicacion
    });
};

bicicletaSchema.methods.toString = function() {
    return `code ${this.code} | color: ${this.color}`;
};

bicicletaSchema.statics.Allbicis = function(cb) {
    return this.find({}, cb);
};

bicicletaSchema.statics.Add = function(aBici, cb) {
    return this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('bicicleta',bicicletaSchema );

// var bicicleta = function (id, color, modelo, ubicacion) {

//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;

// }

// bicicleta.prototype.toString = function () {
//     return 'id' +  this.id + ' | color:' + this.color;
// }

// bicicleta.Allbicis = [];

// bicicleta.Add = function(aBici) {
//     bicicleta.Allbicis.push(aBici);
// }

// bicicleta.findById = function(aBiciId) {
//     var aBici = bicicleta.Allbicis.find(x => x.id == aBiciId);
//     if (aBici)
//         return aBici;
//     else
//         throw new Error(`No existe una bicicleta con el id ${aBiciId}`);

// }

// bicicleta.removeById = function(aBiciId) {
//     // bicicleta.findById(aBiciId);
//     for (var i = 0; i < bicicleta.Allbicis.length; i++) {
//         if(bicicleta.Allbicis[i].id == aBiciId){
//             bicicleta.Allbicis.splice(i, 1);
//             break;
//         }
//     }
// }

// var a = new bicicleta(1, 'Verde', 'Urbana', [6.270459938093382, -75.56358362381951]);
// var b = new bicicleta(2, 'Azul', 'Urbana', [6.263890472627351, -75.56354070847553]);

// bicicleta.Add(a);
// bicicleta.Add(b);

// module.exports = bicicleta;