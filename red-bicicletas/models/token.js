const mongoose =  require('mongoose');

const Schema =  mongoose.Schema;

const tokenSchema=  new Schema({
    _userId: { type: mongoose.Schema.Types.ObjectId, require: true, ref: 'usuario'},
    token: { type: String, require: true },
    createdAt: { type: Date, require: true, default: Date.now, expires: 43200}
});