var mongoose = require('mongoose');
var moment =  require('moment');
var schema = mongoose.Schema;

var reservaSchema = new schema({
    desde: Date,
    hasta: Date,
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'},
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
});

reservaSchema.method.diasDeReserva = function() {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

module.exports = mongoose.model('reserva', reservaSchema);