var map = L.map('main_map').setView([6.265451 , -75.564949], 13);

// L.tileLayer('https://{s}.tileopenstreetmap.org/{z}/{x}/{y}.png',{
//     attribution: '&copy: <a href="https://www.openstreetmap.org/copyright">OpenStreetMap></a> contributors'
// }).addTo(map);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', 
{foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'}).addTo(map);


// L.marker([6.270459938093382, -75.56358362381951]).addTo(map);
// L.marker([6.268156368711809, -75.56931282224343]).addTo(map);
// L.marker([6.263890472627351, -75.56354070847553]).addTo(map);


$.ajax({
    datatype: "json",
    url: "api/bicicleta",
    success: function(result) {
        console.log(result)
        result.bicicleta.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})