var usuario = require('../models/usuarios');
var token = require('../models/token');

module.exports = {
    confirmationGet: function(req, res, next) {
        token.findOne({ token: req.params.token}, function(err, token) {
            if(!token) return res.status(400).send({ type: 'no-verified', msg: 'No se encuentra un usuario con este token, Quizas haya expirado y debas solicitar uno nuevo'});
            usuario.findById(token._userId, function(err, usuario) {
                if(!usuario) return res.status(400).send({ msg: 'no encontramos un usuario con este toke'});
                if(usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save(function(err) {
                    if (err) { return res.status(500).send({msg: err.message}); }
                    res.redirect('/');
                });
            });
        });
    },
}